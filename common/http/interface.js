/**
 * 通用uni-app网络请求
 * 基于 Promise 对象实现更简单的 request 使用方式，支持请求和响应拦截
 */
import { mapMutations } from'vuex'
export default {
	...mapMutations('m_user',['updateUserInfo','updateUserKey']),
	// 请求配置中心
	config: {
	    // 服务器url: "http://外网IP地址:端口号/"  
	    // 本机url: "http://localhost:8090/"
		baseUrl: "http://localhost:8090/", 
		// 请求头
		header: {
			'Content-Type': 'application/json;charset=UTF-8'
			// 'Content-Type': 'application/x-www-form-urlencoded'
		},
		// 请求携带的数据
		data: {},
		// 请求默认方法为GET请求
		method: "GET",
		// 数据类型为JSON
		dataType: "json",
		/* 如设为json，会对返回的数据做一次 JSON.parse */
		responseType: "text",
		// 请求成功后调用的方法
		success() {},
		// 请求失败后调用的方法
		fail() {},
		// 请求完成时调用的方法
		complete() {}
	},
	// 请求和响应拦截器
	interceptor: {
		// request:null,
		// response:null
		refreshTokenInvaild(){
			uni.removeStorageSync("access_token")
			uni.removeStorageSync("refresh_token")
			uni.removeStorageSync("userId")
			this.updateUserInfo(null),
			// this.updateUserKey(null)
			uni.showToast({
				icon:"none",
				title:"双token失效，请重新登录",
				duration:2000
			})
			// return new Promise((resolve) => {
			// 	setTimeout(function(){
			// 		resolve(1)
					uni.reLaunch({
						url:'/pages/my/my'
					})
				// },1000)
			// })
		},
		//请求拦截
		request: (config) => {
			//登录请求
			if(config.url == config.baseUrl+'user/wechat'){
				config.header = {}
				console.log("request登录请求")
			}
			//刷新token请求
			else if(config.url == config.baseUrl+'token/refresh'){
				config.header = {
					"refresh_token": uni.getStorageSync("refresh_token")
				}						
			}
			else{
				config.header = {
					"access_token": uni.getStorageSync("access_token")
				}
				console.log("常规请求")
			}

		},
		//响应拦截
		async response(response) {
			console.log("response请求结束后拦截器")
			 // console.log(response.config)
			// 设置请求结束后拦截器
			if (response.data.status === 200) {//通过
				console.log('响应拦截200')
				return response
			} else if(response.data.status === 401){//access_token失效，先刷新token再请求
				console.log("access_token失效")
				
				await this.refreshAccessToken()
				
					let [err,suc] = await uni.request({
						url: response.config.url,
						data: response.config.data,
						method: response.config.method,
						header:{access_token: uni.getStorageSync("access_token")},
						
					})
					// console.log(suc)
					return suc
			} else {
				uni.showToast({
					title: '错误',
					icon: 'none',
					duration: 2000,
					mask: true
				})
			}
			return response
		},
		refreshAccessToken(){
			return new Promise((resolve,reject) => {
				uni.request({
					url:'http://localhost:8090/token/refresh',
					data: {refreshToken: uni.getStorageSync("refresh_token")},
					method:'POST',
					success: (res) => {
						console.log(res,666666);
						if(res.data.status == 401){
							uni.clearStorage()
							uni.$showMsg('登录过期，请重新登录！',2000,"none")
							this._time = setTimeout(() => {
								uni.reLaunch({
									url:'/pages/my/my'
								})								
								clearTimeout(this._time);
							}, 1000);

						}else{
							if(JSON.parse(res.data.data).refresh_token){
								console.log("自动刷新refresh_token")
								uni.setStorageSync('refresh_token', JSON.parse(res.data.data).refresh_token)
							}
							console.log("自动刷新access_token")
							uni.setStorageSync('access_token', JSON.parse(res.data.data).access_token)
							resolve(res)							
						}
						//如果有refresh_token传来则写入本地缓存

					},
					fail: (err) => {
						reject(err)
					}
				})
			})
		}
	},
	// 封装官方文档提供的API：uni.request
	request(options) {
		if (!options) {
			options = {}
		}
		// 请求配置参数
		options.baseUrl = options.baseUrl || this.config.baseUrl
		options.dataType = options.dataType || this.config.dataType
		options.url = options.baseUrl + options.url
		options.data = options.data || {}
		options.method = options.method || this.config.method
		
		// 返回Promise
		return new Promise((resolve, reject) => {
			let _config = null
			// 设置响应拦截器
			options.complete = (response) => {
				let statusCode = response.statusCode
				response.config = _config		
				if (this.interceptor.response) {
					let newResponse = this.interceptor.response(response)
					if (newResponse) {
						response = newResponse
					}
				}
				// 请求成功
				if (statusCode === 200) { 
					resolve(response);
				} else { // 请求失败
					uni.showToast({
						title: "未知错误",
						icon: 'error',
						duration: 2000,
						mask: true
					})
					reject(response)
				}
			}
			// 请求配置
			_config = Object.assign({}, this.config, options)
			_config.requestId = new Date().getTime()
			// 设置请求拦截器
			if (this.interceptor.request) {
				this.interceptor.request(_config)
			}
			// 调用官方文档提供的API，发起请求
			uni.request(_config);
		});
	},
	// 以下请求可以单独调用
	// GET 请求
	get(url, data, options) {
		if (!options) {
			options = {}
		}
		options.url = url
		options.data = data
		options.method = 'GET'
		return this.request(options)
	},
	// POST 请求
	post(url, data, options) {
		if (!options) {
			options = {}
		}
		options.url = url
		options.data = data
		options.method = 'POST'
		return this.request(options)
	},
	// PUT 请求
	put(url, data, options) {
		if (!options) {
			options = {}
		}
		options.url = url
		options.data = data
		options.method = 'PUT'
		return this.request(options)
	},
	// DELETE 请求
	delete(url, data, options) {
		if (!options) {
			options = {}
		}
		options.url = url
		options.data = data
		options.method = 'DELETE'
		return this.request(options)
	}
}
