/**
 * 将业务所有接口统一起来便于维护
 */
import http from './interface'

export const test = (data) => {
	// 设置请求拦截器
	http.interceptor.request = (config) => {
		console.log('请求拦截', config)
		config.header = {
			// 在发起请求之前，让请求的header携带TOKEN
 			"token": uni.getStorageSync('token')
 		}
	}
	// 设置响应拦截器
	http.interceptor.response = (response) => {
		if (response.code === 200) {
			// 请求成功的执行逻辑
			console.log('响应拦截', response)			
		} else { // 请求失败的执行逻辑
			uni.showToast({
				title: response.msg,
				icon: 'none',
				duration: 2000,
				mask: true
			})
		}				
		return response;
	}
	// 发起test请求
	return http.request({
		url: 'test/wen1',
		method: 'POST',
		data: data
	})
}

/** 
 *  登录业务
 */
export const login = (username, password) => {
	// 设置响应拦截器
	http.interceptor.response = (response) => {
		console.log("响应拦截器")
		console.log(response)
		// 登录成功，则将两个 token 存在用户手机磁盘上，并跳转到首页
		if (response.statusCode == 200) {
			uni.setStorageSync('token', response.data.atoken)
			uni.setStorageSync('refresh_token', response.data.btoken)
			
			// uni.switchTab({
			// 	url: '/pages/home/home'
			// })
		} else { // 登录失败则弹窗提示相应消息给用户
			uni.showToast({
				// title: response.msg,
				title: "response.msg",
				icon: 'none',
				duration: 2000,
				mask: true
			})
		}
		return response;
	}
	// 发起登录请求
	return http.request({
		url: 'test/wen1',
		data: {
			username,
			password
		},
		method: 'POST',
	})
}

// 默认全部导出  import api from '@/common/http/api'
export default {
	test,
	login
}
