const baseUrl = "http://localhost:8090/"

const header = {
	// token: uni.getStorageSync("token")
	refresh_token: uni.getStorageSync("refresh_token")
}

const http = (url ='',data = {},method = "GET") => {
	return new Promise((resolve,reject)=>{
		uni.request({
			url:baseUrl+url,
			header:header,
			data:data,
			method:method,
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}

export default http