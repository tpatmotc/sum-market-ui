import App from './App'
import http from './utils/http.js'
import store from './store/store.js'
import api from './common/http/api.js'
import myApi from './common/http/interface.js'


// #ifndef VUE3
import Vue from 'vue'

//封装弹框的方法
uni.$showMsg = function(title = '数据请求失败！',duration = 1500,icon = 'none'){
	uni.showToast({
		title,
		duration,
		icon
	})
}

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App,
	http,
	api,
	store,
	myApi
})
Vue.prototype.$http = http
Vue.prototype.$api = api
Vue.prototype.$myApi = myApi
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'

//封装弹框的方法
uni.$showMsg = function(title = '数据请求失败！',duration = 1500,icon = 'none'){
	uni.showToast({
		title,
		duration,
		icon
	})
}

export function createApp() {
  const app = createSSRApp(App).use(store)
  app.config.globalProperties.$http = http;
  app.config.globalProperties.$api = api;
  return {
    app
  }
}
// #endif