export default {
	namespaced: true,
	
	state:() => ({
		// address:1111,
		address:JSON.parse(uni.getStorageSync('address') || '{}'),
		//用户头像，昵称
		userinfo: uni.getStorageSync('userinfo') || '',
		//用户姓名、电话、学号......
		userPrivateInfo: uni.getStorageSync('userPrivateInfo') || '',
		//用户ID
		userKey: uni.getStorageSync('userKey') || '',
		//重定向的 object 对象 { openType, from }
		redirectInfo: null
	}),

	mutations:{
		//更新收货地址
		updateAddress(state,address){
			state.address = address
			this.commit('m_user/saveAddressToStorage')
		},
		//持久化存储address
		saveAddressToStorage(state){
			uni.setStorageSync('address',JSON.stringify(state.address))
		},
		updateUserInfo(state,userinfo){
			state.userinfo = userinfo
			this.commit('m_user/saveUserInfoToStorage')
		},
		saveUserInfoToStorage(state){
			uni.setStorageSync('userinfo',state.userinfo)
		},
		updateUserPrivateInfo(state,userPrivateInfo){
			state.userPrivateInfo = userPrivateInfo
			this.commit('m_user/saveUserPrivateInfoToStorage')
		},
		saveUserPrivateInfoToStorage(state){
			uni.setStorageSync('userPrivateInfo',state.userPrivateInfo)
		},
		updateUserKey(state,userKey){
			state.userKey = userKey
			this.commit('m_user/saveUserKey')
		},
		saveUserKey(state){
			uni.setStorageSync('userKey',state.userKey)
		},
		updateRedirectInfo(state,info){
			state.redirectInfo = info
		}
	},
	
	getters:{
		//收货地址
		addstr(state){
			if(!state.address.provinceName) return ''
			
			return state.address.provinceName + state.address.cityName + state.address.countyName + state.address.detailInfo
		}
	}
}