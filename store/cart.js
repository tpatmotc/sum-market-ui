export default {
  // 为当前模块开启命名空间
  namespaced: true,

  // 模块的 state 数据
  state: () => ({
    // 购物车的数组，用来存储购物车中每个商品的信息对象
    cart: JSON.parse(uni.getStorageSync('cart') || '[]')
  }),

  // 模块的 mutations 方法
  mutations: {
	      addToCart(state, good) {
	        // 根据提交的商品的Id，查询购物车中是否存在这件商品
	        // 如果不存在，则 findResult 为 undefined；否则，为查找到的商品信息对象
	        const findResult = state.cart.find((x) => x.good_id == good.good_id)
	  
	        if (!findResult) {
	          // 如果购物车中没有这件商品，则直接 push
	          state.cart.push(good)
			  uni.$showMsg("添加成功！",800,'success')
			  // console.log(good)
	        } else {
				  // 如果购物车中有这件商品，则只更新数量即可
				if(findResult.good_count>=findResult.good_stock)
					uni.$showMsg('已达数量上限!',1000,'error')
				else{
					// console.log("count="+findResult.good_count)
					// console.log("quantitly="+findResult.good_stock)
					findResult.good_count = findResult.good_count + good.good_count;
					uni.$showMsg("添加成功！",800,'success')
				}
	        }
			//通过 commit 方法，调用 m_cart 命名空间下的 saveToStorage 方法
			this.commit('m_cart/saveToStorage')
	      },
		  saveToStorage(state){
			  uni.setStorageSync('cart',JSON.stringify(state.cart))
		  },
		  removeEquipmentById(state,good_id){
		  	state.cart = state.cart.filter(x => x.good_id !== good_id)
		  	this.commit('m_cart/saveToStorage')
		  },
		  updateEquipmentRadioSwitch(state,good){
		  	const findResult = state.cart.find(x => x.good_id === good.good_id)
		  	if(findResult){
		  		findResult.good_radioSwitch = good.good_radioSwitch
		  		this.commit('m_cart/saveToStorage')
		  	}
		  },
		  //全选、反选
		  updateAllEquipmentRadioSwitch(state,newState){
			  state.cart.forEach(x => x.good_radioSwitch = newState)
			  this.commit('m_cart/saveToStorage')
		  },
		  //更新设备数量
		  updateEquipmentCount(state,good){
		  	const findResult = state.cart.find(x => x.good_id === good.good_id)
		  	if(findResult){
		  		findResult.good_count = good.good_count
		  		this.commit('m_cart/saveToStorage')
		  	}
		  },
		  //更新商品状态，每次点开购物车自动刷新商品状态
		  async updateEquipmentState(state){
			  const db = uniCloud.database();
			  var i;
			  for(i=0;i<state.cart.length;i++){
				  await db.collection('yun-equipment')
				  .where({
					  eq_id:state.cart[i].equipment_id
				  }).get()
				  .then((res) =>{
					  // console.log(res.result.data[0].eq_status)
					  state.cart[i].equipment_status = res.result.data[0].eq_status;
					  state.cart[i].equipment_status_type = res.result.data[0].eq_status_type;
					  this.commit('m_cart/saveToStorage')
				  }).catch(err =>{
					  console.log(err)
				  })
			  }
		  }
  },

	// 模块的 getters 属性
	getters: {
	   // 统计购物车中商品的总数量
	   total(state) {
		  let c = 0
		  // 循环统计商品的数量，累加到变量 c 中
		  state.cart.forEach(good => c += good.good_count,0)
		  return c
	   },
	   // 勾选的商品的总数量
	   checkedCount(state){
		     // 先使用 filter 方法，从购物车中过滤器已勾选的商品
		     // 再使用 reduce 方法，将已勾选的商品总数量进行累加
		     // reduce() 的返回值就是已勾选的商品的总数量
			return state.cart.filter(x => x.good_radioSwitch).reduce((total,item) => total += item.good_count,0)
	   },
	   //已勾选商品的总价格
	   checkedGoodsAmount(state){
	   	return state.cart.filter(x => x.good_radioSwitch).reduce((total,item) => total += item.good_count * item.good_price, 0).toFixed(2)
	   }
	}
}